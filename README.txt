CONTENTS OF THIS FILE
---------------------
 * Summary
 * Dependencies
 * Usage

Summary
------------

Allow to capture and log content locking provided by the content_lock module.

Dependencies
------------

- content_lock


Usage
------
- Enable content lock log module.
- Grant users the 'view lock log' permission.
- Visit node/[nid]/locklog.