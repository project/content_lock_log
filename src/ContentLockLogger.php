<?php

namespace Drupal\content_lock_log;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Cache\Cache;

/**
 * Class ContentLockLogger.
 */
class ContentLockLogger extends ServiceProviderBase {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   *   The database service.
   */
  protected $database;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The date.formatter service.
   */
  public function __construct(Connection $database, AccountInterface $current_user) {
    $this->currentUser = $current_user;
    $this->database = $database;
  }


  /**
   * Save an entry in the database.
   * @return \Drupal\Core\Database\StatementInterface|null
   *   The number of updated rows.
   *
   * @throws \Exception
   *   When the database insert fails.
   *
   */
  protected function insert(array $entry, $op) {
    $return_value = NULL;
    $entry['op'] = $op;
    $entry['timestamp'] = \Drupal::time()->getRequestTime();
    $entry['uid'] = $this->currentUser->id();
    try {
      $return_value = $this->database->insert('content_lock_log')
        ->fields($entry)
        ->execute();
    } catch (\Exception $e) {
      drupal_set_message(t('db_insert failed. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]
      ), 'error');
    }
    // Invalidate the lock log render cache.
    Cache::invalidateTags(["content_lock_log:node:" . $entry['entity_id']]);
    return $return_value;
  }

  /**
   * Log a lock entry in the database.
   * @return \Drupal\Core\Database\StatementInterface|null
   *   The number of updated rows.
   *
   * @throws \Exception
   *   When the database insert fails.
   *
   */
  public function logLock($entity_id, $entity_type) {
    $entry = ['entity_id' => $entity_id, 'entity_type' => $entity_type];
    return $this->insert($entry, 'lock');
  }

  /**
   * Log a release in the database.
   * @return \Drupal\Core\Database\StatementInterface|null
   *   The number of updated rows.
   * @throws \Exception
   */
  public function logRelease($entity_id, $entity_type) {
    $entry = ['entity_id' => $entity_id, 'entity_type' => $entity_type];
    return $this->insert($entry, 'release');
  }

  /**
   * Delete log entries for an entity from the database.
   *
   * @param int $entity_id
   *  The entity id.
   */
  public function delete(int $entity_id) {
    $this->database->delete('content_lock_log')
      ->condition('entity_id', $entity_id)
      ->execute();
  }

  /**
   * Load content_lock_log records joined with user records.
   *
   * @see $database->select()
   */
  public function contentLockLogLoad($entity_id, $headers) {
    $select = $this->database->select('content_lock_log', 'c');
    // Join the users table, so we can get the entry creator's username.
    $select->join('users_field_data', 'u', 'c.uid = u.uid');

    $select->addField('u', 'name', 'username');
    $select->addField('c', 'op');
    $select->addField('c', 'timestamp');
    // Filter by entity id.
    $select->condition('c.entity_id', $entity_id);
    $pager = $select->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(50)
      ->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($headers);

    $entries = $pager->execute()->fetchAll(\PDO::FETCH_ASSOC);

    return $entries;
  }

}
