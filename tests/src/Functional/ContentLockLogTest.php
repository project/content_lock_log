<?php

namespace Drupal\Tests\content_lock_log\Functional;

use Drupal\Tests\BrowserTestBase;
/**
 * Tests for the content_lock_log module.
 *
 * @group content_lock_log
 *
 * @ingroup content_lock_log
 */
class ContentLockLoggerTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['content_lock','content_lock_log'];

  /**
   * The installation profile to use with this test.
   *
   * We need the 'minimal' profile.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * User object for our test.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webUser;

  /**
   * Verify that current user has no access to log.
   *
   * @param string $url
   *   URL to verify.
   */
  public function contentLockLogVerifyNoAccess($url) {
    // Test that page returns 403 Access Denied.
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Main test.
   *
   * Login user, get a node id, and test lock log page functionality.
   *
   * @TODO test lock and release on node.
   */
  public function testContentLockLog() {
    $assert_session = $this->assertSession();

    // Create a authenticated user and login.
    $this->webUser = $this->drupalCreateUser();
    $this->drupalLogin($this->webUser);

    // Query for a node id.
    $select = db_select('node', 'n')
      ->fields('n', ['nid'])
      ->addMetaData('account', $this->webUser)
      ->addTag('node_access')
      ->limit(1);
    $nid = $select->execute()->fetch()->nid;
    $path = 'node/' . $nid . '/locklog';

    // Verify that regular user can't access the pages created.
    $this->contentLockLogVerifyNoAccess($path);

    // Create a user with permissions to access locklog.
    $this->webUser = $this->drupalCreateUser(['view lock log']);
    $this->drupalLogin($this->webUser);

    // Verify that user can access simple content.
    $this->drupalGet($path);
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('Content Lock Log');
  }

}
