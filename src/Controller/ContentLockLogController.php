<?php

namespace Drupal\content_lock_log\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\content_lock_log\ContentLockLogger;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Controller for Content Lock Log.
 */
class ContentLockLogController extends ControllerBase {
  /**
   * The lock logger service.
   *
   * @var \Drupal\content_lock_log\ContentLockLogger
   */
  protected $lockManager;

  /**
   * The date.formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   *   The date.formatter service.
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $lockManager = $container->get('content_lock_log_logger');
    $dateFormatter = $container->get('date.formatter');
    return new static($lockManager, $dateFormatter);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ContentLockLogger $lockManager, DateFormatter $dateFormatter) {
    $this->lockManager = $lockManager;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * Generates lock log display.
   *
   * @param \Drupal\node\NodeInterface $entity
   *   The node to export.
   *
   * @return array
   *   A render array.
   *
   * @TODO: refactor this to make it work with all entities.
   */
  public function viewLockHistory(NodeInterface $node) {
    $content = array(
      '#entity' => $node,
      '#sorted' => TRUE,
      '#pre_render' => [[$this, 'buildList']],
      // Cache the render array until the lock history/node is changed.
      '#cache' => [
        'contexts' => ['url.query_args'],
        'keys' => ['content_lock_log', $node->id()],
        'tags' => ['content_lock_log:node:' . $node->id()],
      ],
    );

    return $content;
  }


  /**
   * Builds an entity's lock log.
   *
   * This function is assigned as a #pre_render callback in ::viewLockHistory().
   *
   * It transforms the renderable array and generate a table of lock logs.
   *
   * @param array $build
   *   A renderable array containing build information.
   *
   * @return array
   *   The updated renderable array.
   *
   * @see drupal_render()
   */
  public function buildList($build) {
    // Build a sortable table.
    $headers = [
      ['data' => t('User'), 'field' => 'username'],
      ['data' => t('Operation'), 'field' => 'op'],
      ['data' => t('Date/Time'), 'field' => 'timestamp', 'sort' => 'desc']
    ];
    $build['table'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#empty' => t('No entries available.'),
    ];

    // Add a pager to avoid loading too many entries.
    $build['pager'] = [
      '#type' => 'pager',
      '#weight' => 1,
    ];

    $rows = [];
    // Load the lock history.
    $entries = $this->lockManager->contentLockLogLoad($build['#entity']->id(), $headers);
    foreach ($entries as $key => $entry) {
      // Sanitize each entry.
      $rows[] = array_map('Drupal\Component\Utility\SafeMarkup::checkPlain', (array) $entry);
      $rows[$key]['timestamp'] = $this->dateFormatter->format($entry['timestamp']);
    }
    $build['table']['#rows'] = $rows;

    return $build;
  }
}

